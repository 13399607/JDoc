package com.nmtx.doc.core.parser;

import java.util.List;

import com.nmtx.doc.bean.Api;
import com.nmtx.doc.core.api.ApiBuilder;
import com.nmtx.doc.core.api.AbstractApiParser;
import com.nmtx.doc.core.formater.Formater;
import com.nmtx.doc.core.formater.FormaterBuilder;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.RootDoc;

/**
 * 文档解析器
 * 
 * @author lianghao
 *
 */
public class DocumentParser {

   
    public static boolean start(RootDoc root) {
        AbstractApiParser apiParser = ApiBuilder.builder();
        
        ClassDoc[] classDocs = root.classes();
        
        List<Api> apis = apiParser.generateApi(classDocs);
        
        Formater formater = new FormaterBuilder().builder();
        
        formater.output(apis);

        return true;
    }

    
}
