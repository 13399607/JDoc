#JDoc
JAVA 接口文档生成利器 JDoc

JDoc是基于JAVA开发的，针对主流web框架做的接口文档生成利器，只需极简配置就可以实现文档的生成，且对项目零入侵，主要目的是解决协作开发中接口文档及时更新，开发JDoc的想法来源于项目XDoc,项目地址https://git.oschina.net/treeleaf/xDoc 一直想开发一个文档生成器，没有找到合适的解决方法，项目中用到的javadoc给我了很大的帮助，在此感谢XDoc作者-风里的叶子

JDoc有如下主要特点

1.简单易用，配置极简

2.对项目零入侵，在不改变项目本身代码结构上加上JDoc的格式注解就可以快速生成文档，只对Controller层解析

JDoc使用

maven包依赖,javadoc.jar在工程lib目录下
```
<dependency>
    <groupId>com.nmtx</groupId>
    <artifactId>jdoc</artifactId>
    <version>1.0</version>
</dependency>

<dependency>
      <groupId>sun</groupId>
      <artifactId>javadoc</artifactId>
      <version>1.0</version>
</dependency>
```
JDoc配置文件,参考配置如下，配置文件放在resource文件夹中，名字为jdoc.properties

```
#指定java文件路径
java.source=src/test/java

#指定需要生成文档的包路径
package.name=com.nmtx.test

#指定采用的框架类型,目前支持jfinal,springmvc两种框架
parser.name=jfinal

#指定文档输出路径
out.path=test.html

#指定模版生成器，目前仅支持html
parser.formater=html
```
Jdoc注解格式如下
类注释

```
/**
 * 我是测试controller
 * 
 * @author lianghao
 *
 *         2017年3月23日
 */
```

我是测试为一级文档标题

方法注释

```
     /**
     * 我是测试接口
     * 
     * @param username|用户名|Integer|必填
     * @title 测试接口
     * @respParam username|用户名|Integer|必填
     * @respBody {username:23234}
     */
```
@param为请求参数 可设置多个 参数名 描述 请求类型 是否必填 

@title 接口标题 

@respParam返回参数 参数名 描述 请求类型 是否必填  可设置多个

@respBody返回demo 



 **JFinal框架中使用，因JFinal本身架构的原因，外部无法拿到对应根映射，JDoc提供两种方法解决** 

1.第一种配置，清除后缀方法，比如说TestController，默认访问链接为/test
```
	/**
	 * 配置访问路由
	 */
	@Override
	public void configRoute(Routes me) {
		//TODO 配置路由
                new JFinalApiDocConfig().setClearSuffix("Controller").start();
	}
 

```
2.第二种配置，与路由配置一致，调用add方法把路由加进去
```
new JFinalApiDocConfig().setUseClearSuffix(false).add("/jfinal", JFinalController.class).start();
```

 **SpringMVC，SpringBoot框架中使用如下** 

```
package com.nmtx.springmvc.doc.config;

import com.nmtx.doc.core.api.springmvc.SpringMVCApiDocConfig;

public class SpringDocBuilder {

    public static void main(String[] args) {
        SpringMVCApiDocConfig doc = new SpringMVCApiDocConfig();
        doc.setConfigFilePath("jdoc.properties");
        doc.start();
    }
}

```
SpringMVC demo地址 https://git.oschina.net/lianghao2016/springmvc-jdoc-demo

JFinal demo地址 https://git.oschina.net/lianghao2016/jdoc-jfinal-demo

api文档截图
![输入图片说明](http://git.oschina.net/uploads/images/2017/0328/151545_15cbbe05_383641.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0328/151559_4f7731fb_383641.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0328/151627_c1780a4c_383641.png "在这里输入图片标题")